# Utilizar Debian 11 como base
FROM debian:11

# Metadatos como autor y descripción
LABEL maintainer="DSL <shackleton@riseup.net>" \
      description="Contenerizacion de una webapp"

# Establecer el shell en modo no interactivo para evitar interacciones durante la instalación
ENV DEBIAN_FRONTEND=noninteractive

# Actualizar la lista de paquetes y instalar dependencias básicas
RUN apt-get update && apt-get upgrade -y && \
    apt-get install -y curl gnupg build-essential ruby-full bundler

# Copiar todos los archivos del directorio actual al contenedor
COPY . /dsl

# Establecer el directorio de trabajo
WORKDIR /dsl/app

# Ejecutar bundle install para instalar las dependencias de Ruby (opcional si tienes un Gemfile)
RUN bundle install

EXPOSE 4000
